---?image=assets/images/Go-Logo_White.png&position=50% 20%&size=50% 50%
@title[Introduction]

<br/>
<br/>
<br/>
<br/>
A *simple* statically typed compiled language.

---?color=#ffffff&image=assets/images/gopher_computer.gif&position=50% 70%&size=35% 35%&opacity=80
## @color[black](Start your engines)
<br/>
<br/>
<br/>
<br/>
<br/>

---
## Table of Contents

* Introduction
* Why Go?
* Golang Basics
* <- coffee break!
* More Data Structures
* Golang Goodies

---
## Introduction

+++?color=#000000&image=assets/images/emc2.png&position=50% 60%&size=50% 50%&opacity=90

<br/>
<br/>
<br/>
<br/>

+++
### Costs vs Features

<canvas data-chart="line">
Complexity,0,2.5,7,12.5
Bugs,0,3,7,13
Usefulness,0,30,55,68
<!--
{
 "data": {
  "labels": ["Start","","","","","","","","","Features"],
  "datasets": [
  {"pointRadius":0, "borderColor":"green", "backgroundColor":"green", "fill": false},
  {"pointRadius":0, "borderColor":"orange", "backgroundColor":"orange", "fill": false},
  {"pointRadius":0, "borderColor":"blue", "backgroundColor":"blue", "fill": false}
  ]},
 "options": {
            "responsive": "true",
            "animation": {"duration": 10000},
            "scales": {"yAxes": [{"display":false, "ticks":{"max":120}}]}
            }
}
-->
</canvas>

+++
### Costs vs Features

<canvas data-chart="line">
Complexity,0,2.5,7,12.5,20,32.5,48
Bugs,0,3,7,13,30,60,90
Usefulness,0,30,55,68,73,70,63
<!--
{
 "data": {
  "labels": ["Start","","","2nd Stage","","","","","","Features"],
  "datasets": [
  {"pointRadius":0, "borderColor":"green", "backgroundColor":"green", "fill": false},
  {"pointRadius":0, "borderColor":"orange", "backgroundColor":"orange", "fill": false},
  {"pointRadius":0, "borderColor":"blue", "backgroundColor":"blue", "fill": false}
  ]},
 "options": {
            "responsive": "true",
            "scales": {"yAxes": [{"display":false, "ticks":{"max":120}}]}
            }
}
-->
</canvas>

+++
### Costs vs Features

<canvas data-chart="line">
Complexity,0,2.5,7,12.5,20,32.5,48,65,85,110
Bugs,0,3,7,13,30,60,90,110,100,98
Usefulness,0,30,55,68,73,70,63,53,50,53
<!--
{
 "data": {
  "labels": ["Start","","","2nd Stage","","","3rd Stage","","","Features"],
  "datasets": [
  {"pointRadius":0, "borderColor":"green", "backgroundColor":"green", "fill": false},
  {"pointRadius":0, "borderColor":"orange", "backgroundColor":"orange", "fill": false},
  {"pointRadius":0, "borderColor":"blue", "backgroundColor":"blue", "fill": false}
  ]},
 "options": {
            "responsive": "true",
            "scales": {"yAxes": [{"display":false, "ticks":{"max":120}}]}
            }
}
-->
</canvas>

+++
### Costs vs Features

<canvas data-chart="line">
Complexity,0,2.5,7,12.5,20,32.5,48,65,85,110
Bugs,0,3,7,13,30,60,90,110,100,98
Usefulness,0,30,55,68,73,70,63,53,50,53
Max Complexity,30,30,30,30,30,30,30,30,30,30
<!--
{
 "data": {
  "labels": ["Start","","","2nd Stage","","","3rd Stage","","","Features"],
  "datasets": [
  {"pointRadius":0, "borderColor":"green", "backgroundColor":"green", "fill": false},
  {"pointRadius":0, "borderColor":"orange", "backgroundColor":"orange", "fill": false},
  {"pointRadius":0, "borderColor":"blue", "backgroundColor":"blue", "fill": false},
  {"borderDash":[5, 5], "pointRadius":0, "borderColor":"red", "backgroundColor":"red", "fill": false}
  ]},
 "options": {
            "responsive": "true",
            "scales": {"yAxes": [{"display":false, "ticks":{"max":120}}]}
            }
}
-->
</canvas>


+++?image=assets/images/cable_mess.jpg&position=40% 80%&size=50% 50%&opacity=75
### @color[orange](Complexity)
from latin: *com* "together", *plex* "interlaced"

@color[red](internal interactions) (Software)
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>


+++
#### Complexity in Software Development

@ul
* **@color[gray](Essential complexity)**: caused by the characteristics of the problem to be solved
  * @color[yellow](Cannot be reduced)
* **@color[gray](Accidental complexity)**: @color[yellow](everything else)
@ulend

+++
#### Accidental Complexity

@color[yellow](*everything else*)
@ul
* Team communication, task coordination, schedule
* Difficulties with @color[yellow](chosen tools)
  * git vs svn
  * GroupWise vs Mattermost
  * Python vs @color[yellow](C/C++)
@ulend

+++
#### Accidental Complexity

*If more than one function is selected, any function template specializations
in the set are eliminated if the set also contains a non-template function,
and any given function template specialization F1 is eliminated if
the set contains a second function template specialization whose ... *

@color[yellow](C++11, §13.4 [4])

+++?image=assets/images/aliens_c++.png&position=50% 80%&size=80% 80%&opacity=75
#### Accidental Complexity
* a recently observed chat status

```C++
public static <I, O> ListenableFuture<O>
chain(ListenableFuture<I> input, Function<? super
I, ? extends ListenableFuture<? extends O>>
function)
```

+++
### Go Motivation

*But the C++0x talk got me thinking again.
It just felt wrong to put such a @color[gray](microscopically-defined set of details into an already over-burdened type system)*

*We returned to our offices after the talk.
Before the compilation was done, we'd roped Ken in and had decided to do something.*

*@color[gray](We did not want to be writing in C++ forever)*

Rob Pike

+++
#### Accidental Complexity

*The problem with object-oriented languages is they’ve got all this
implicit environment that they carry around with them.*

*@color[gray](You wanted a banana but what you got was a gorilla holding the banana and the entire jungle)*

– Joe Armstrong
(creator of Erlang)

+++?color=#000000&image=assets/images/expanding_brain.jpg&position=50% 70%&size=100% 100%&opacity=60
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
#### @color[yellow](Less is More)

#### @color[red](simple is harder than complex)

---
## Why Go?

+++
### Go Origins

* Go started in 2007 with Rob Pike, Robert Griesemer, Ken Thompson
* Rob Pike: @color[gray](Plan9, UTF-8)
* Ken Thompson: @color[gray](creator of B, first Unix implementation)

+++
### Go Design

*Go aims to combine the @color[yellow](safety and performance) of a
statically typed compiled language (C/C++) with the
@color[yellow](expressiveness and convenience)
of a dynamically typed interpreted language (Python/Ruby)
suitable for modern @color[yellow](system programming)*

+++?color=#000000&image=assets/images/fast_vs_fun.png&position=50% 50%&size=80% 80%

+++?color=#000000&image=assets/images/single_thread_performance_vs_freq.png&position=50% 70%&size=60% 60%
### Hardware Limitations
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

+++?color=#000000&image=assets/images/go_cpp_c_trend.png&position=50% 70%&size=60% 60%
### Popularity
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>


+++?color=#000000&image=assets/images/concurrency_efficient_vs_beautiful.png&position=50% 50%&size=80% 80%

+++
### Design Principles

@ul
* Fast compilation
* System programming capable
  * Fast @color[gray](: compiled, not interpreted)
  * Expressive type system @color[gray](: with static safety check)
* Concurrency
* Garbage collection
* Cross-Platform
@ulend

+++
#### Fast Compilation

* If @color[red](A.go) depends on @color[blue](B.go) depends on @color[orange](C.go):
  * compile @color[orange](C.go), @color[blue](B.go), then @color[red](A.go)
  * to compile @color[red](A.go), compiler reads @color[blue](B.o) but not @color[orange](C.o).


@color[yellow](Each compiled package file imports transitive dependency info)

+++
#### Fast Compilation

* C: `#include <stdio.h>`

reads 360 lines from 9 files

* C++: `#include <iostream>`

reads 25,326 lines from 131 files

* Go: `import "fmt"` reads @color[yellow](one file):

195 lines summarizing 6 dependent packages.

+++?color=#000000&image=assets/images/compilation_time.png&position=50% 70%&size=80% 80%
#### Fast Compilation
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

+++?color=#000000&image=assets/images/jvm_vs_binary.png&position=50% 70%&size=60% 60%
#### System Programming
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

+++
#### Like C, Go has

- full set of unsigned types
- bit-level operations
- programmer control of memory layout

```Go
type T struct {
x
int
buf [20]byte
...
}
```

- pointers, @color[yellow](but no pointer arithmetics)

```Go
p := &t.buf
```

+++
#### Concurrency

* Go is @color[yellow](concurrent), not parallel
  * concurrency: @color[yellow](dealing with) lots of things at once
  * parallelism: @color[yellow](doing) lots of things at once


Go provides independently executing @color[yellow](goroutines) that
communicate and synchronize using @color[yellow](channels)

```Go
    for i := 0; i < 10; i++ {
		go dealWithSomething() // goroutine
	}
```

+++?color=#000000&image=assets/images/go_routines.gif&position=50% 80%&size=40% 40%
#### Concurrency

@color[yellow](Processing context[P]) can switch to another @color[yellow](OS thread[M])
if the actual one is stopped, eg: syscalls

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

+++
#### Expressive Type System

* Go is object-oriented, BUT:
  * no classes
  * no subclassing
* @color[yellow](Any types), even basic types such as integers and strings,
can have methods.

```Go
type Temperature float

func (t Temperature) String() string {
    return fmt.Sprintf("%.1f°C", t)
}
```

@[1](declare custom type based on float)
@[2-5](custom method for Temperature)


+++
#### Garbage Collection: Debate

@ul
* C: painfully manage memory allocation and release.
* C++: easier allocation.
  * Smart pointers automate memory release
  * Smart pointers are @color[yellow](optional) (though they shouldn't be)
* Why not just use garbage collection?
  * Memory release *predictability* (Do we actually need this?)
  * Stop-the-world gc pause times too long
@ulend

+++?color=#000000&image=assets/images/gc_benchmark_1.3_1.5.png&position=50% 90%&size=50% 50%
#### Garbage Collection: Is it fast enough?

* Hybrid GC: Concurrent + Stop-the-world
* Optimized for @color[yellow](low latency)
* Customizable (can be adjusted for latency or throughput)
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

+++?color=#000000&image=assets/images/gc_go_1.6.png&position=50% 90%&size=60% 60%
#### GC: Is it *actually* fast enough?

@color[yellow](200GB) heap - @color[yellow](~20ms) pause time (Go 1.6, 2016)
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

+++?color=#000000&image=assets/images/twitter_gc_1.5.png&position=50% 70%&size=60% 60%

#### update to Go 1.5, August 2015
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

+++?color=#000000&image=assets/images/twitter_gc_1.6.jpeg&position=50% 70%&size=60% 60%

#### update to Go 1.6 rc1, January 2016
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

+++?color=#000000&image=assets/images/twitter_gc_1.6.3.jpeg&position=50% 70%&size=60% 60%

#### update to Go 1.6.3 (gc bug fix), August 2016
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

+++?color=#000000&image=assets/images/twitter_gc_1.7.png&position=50% 70%&size=60% 60%

#### update to Go 1.7, August 2016
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

+++?color=#000000&image=assets/images/twitter_gc_1.8.jpeg&position=50% 75%&size=60% 60%

#### update to Go 1.8 beta 1, December 2016
18GB heap, pause < 1ms
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

+++
#### GC Discussion
##### Do we *actually* need to manage memory?

---
### Golang Quickstart

+++
#### Hello World

```Go
package main
import "fmt"
func main() {
    fmt.Println("hello world")
}
func MyFunction(){} // exported function
func myFunction(){} // NOT exported function
```

@[1](Go is organized into packages)
@[2](import is like *include* from C/C++ and *import* from python)
@[3-5](int main()... there may only be one)
@[4](Use fmt package to print)
@[6-7](Note: CamelCase is **not** trivial, exported values @color[yellow](must) start with capitals)

+++
#### Hello World

```
$ go build hello-world.go
$ ls
hello-world	hello-world.go

$ file hello-world
hello-world: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), statically linked, not stripped
$ ./hello-world
hello world

$ go run hello-world.go
hello world
```

@[1-3](go build compiles)
@[5-8](Elf binary file)
@[10-11](go run *hides* compilation step)

+++
#### Basic Types
```Go
bool
string
int int8 int16 int32 int64
uint uint8 uint16 uint32 uint64
byte // alias for uint8
rune // alias for int32 (unicode char)
float32 float64
complex64 complex128
```

@[1-4](usual types)
@[5](Alias for uint8)
@[6](Unicode char)
@[7-8](Floats....and @color[yellow](complex built-in!))

+++
#### Everything is passed by value
```Go
func foo(test int){
    test = 1 // change the value to 1
}
func bar(test *int){
    *test = 1
}
func main(){
    var i = 0
    fmt.Println(i) // in main: i=0
    foo(i)
    fmt.Println(i) // after foo: i=0
    bar(&i)
    fmt.Println(i) // after bar: i=1
}
```

@[1-14](Like all C family languages, arguments are @color[orange](copies))
@[1-14](Pointers are copies too)

+++
#### Variables
```Go
var foo bool // implicit initialization
var foo, bar int // implicit initialization
foo := string("hello!") // initialized
bar := &foo // initialized
bar2 := *bar // initialized
bar2 := *(bar+1) // illegal
a, b := myFunction() // initialized
const e = 2.718281828459045235360
var myFunction = func(x int) int {return x + e}
```
@[1](Var declares a variable, type is *on the right*)
@[2](Multiple vars can be listed)
@[3](Type can be *deduced*, like `auto`)
@[3-4](What is the type of bar?)
@[3-4](it's a `*string` pointer)
@[3-5](Pointer's underlying value, `bar2` is type `string`)
@[6](@color[red](No pointer arithmetics!))
@[7](Create vars while calling a function!)
@[8](there are constants too!)
@[8-9](functions are first-class values!)
@[2](What is the *value* of foo, bar..?)

+++
#### Zero-Values
```Go
var b bool
var c bool = true
```

* variables without explicit initial value get their @color[yellow](zero value)

  * `0` for numeric types
  * `false` for boolean
  * `""` empty string for strings
  * `nil` for pointers

+++
#### Syntactic Sugar

```Go
var (
    foo uint
    bar string
    bar2 = "hello world!"
    bar3 = 0.45
)

import (
    "fmt"
    "time"
    buf "bufio"
)
```
@[1-6](Multiple types, single *var* statement)
@[3-5](What is the type and value of bar, bar2, bar3?)
@[8-12](Multiple packages in a single *import* statement)
@[11](Package *alias* can be used like `import bufio as buf` (python))

+++
#### Flow Control

```Go
if i < 0 {
    doSomething()
} else {
    doSomethingElse()
}
if v := math.Pow(x, n); v < lim {
		fmt.Println("v is below limit: %v", v)
}
fmt.Println(v) // compilation error
```

@[1-5](simple syntax, {} are required!)
@[6-9](`v` is only scope until the end of the `if`)

+++
#### Flow Control

```Go
for i := 0; i < 10; i++ {
    sum += i
}
for ; sum < 10; {
    sum += i
}
for sum < 10 {
    sum += i
}
for {
    // infinite loop
}
```

@[1-3](simple syntax, {} are required!)
@[1-3](note `i` is created in the @color[yellow](init statement))
@[4-6](@color[yellow](init) and @color[yellow](post) statements are optional)
@[7-12](drop the `;`, C's `while` is called `for` in Go!)

+++
#### Flow Control: Switch
```Go
switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
	default:
		// freebsd, openbsd,
		// plan9, windows...
		fmt.Printf("%s.", os)
	}
t := time.Now()
switch {
	case t.Hour() < 12:
		fmt.Println("Good morning!")
	case t.Hour() < 17:
		fmt.Println("Good afternoon.")
	default:
		fmt.Println("Good evening.")
	}
```

@[1-10](classical switch over a variable, but @color[yellow](not limited to ints))
@[11-19](can make function calls on each switch case)


+++
#### Range

```Go
for i, v := range pow {
    // i is the index
    // v = myArray[i]
    fmt.Printf(v)
}
```

@[1-5](@color[yellow](range) is Go built-in mechanism for iteration)

+++
#### Functions

```Go
func add(x int, y int) int {
	return x + y
}

func add(x, y int) int {
	return x + y
}
```

@[1](Takes @color[yellow](2 ints), returns @color[yellow](1 int))
@[5](Shorter version: x and y are both int)

+++
#### Functions

```Go
func swap(x, y string) string, string {
	return y, x
}
func process(mystr string) string, error {
	return "all good", nil
}
file, err := os.Open("file.go") // For read access.
if err != nil {
	log.Fatal(err)
}
type error interface {
    Error() string
}
```

@[1-3](Functions can return multiple values!)
@[4-6](Second return value, *commonly* used to return errors)
@[5](`nil` = null pointer usually means no error)
@[7-10](Go doesn't have exceptions, errors are managed by returns)
@[11-13](errors are @color[yellow](not ints), they are @color[orange](interfaces))

---
### More Data Structures

+++
#### More data structures

```Go
var example [10]int
primeNumbers := [6]int{2, 3, 5, 7, 11, 13}
selectedPrimes := myArray[:3] // 2, 3, 5
selectedPrimes[0] := 17
fmt.Println(selectedPrimes)
fmt.Println(len(selectedPrimes), cap(selectedPrimes))
fmt.Println(len(selectedPrimes[:4]), cap(selectedPrimes[:4]))

a := make([]int, 5)  // len(a)=5
b := make([]int, 0, 5) // len(b)=0, cap(b)=5
```

@[1](arrays have fixed size)
@[2](arrays can be initialized)
@[2-3](@color[orange](slices) are a @color[yellow](flexible view) into the elements of an array)
@[2-3](@color[orange](slices) have a @color[yellow](pointer to an array), a @color[yellow](capacity)(max) and @color[yellow](length))
@[2-4](What's the value of primeNumbers?)
@[2-5]([17, 3, 5, 7, 11, 13])
@[2-6](`len(selectedPrimes)`: 3, `cap(selectedPrimes)`: 6)
@[2-6](`len(selectedPrimes`@color[yellow]([:4])\): 4, `cap(selectedPrimes`@color[yellow]([:4])\): 6)
@[9-10](how to create a *dynamically-sized* array)
@[9-11](a and b are slices!)

+++?color=#000000&image=assets/images/slice_representation.jpg&position=50% 75%&size=60% 60%

#### But what are slices?
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

+++
#### Everything is passed by value...?
```Go
func foo(b [2]byte) { // takes an array
	b[1] = 0
}
func bar(b []byte) { // takes a slice
	b[1] = 0
}
func main() {
	array := [2]byte{1, 2} // [1 2] original
	foo(array)
	fmt.Println(array) // [1 2] unchanged!
	bar(array[:])
	fmt.Println(array) // [1 0] changed!
}
```

@[1-10](arrays are copied)
@[1-15](slices are passed by "reference", @color[orange](it changes!))

+++?color=#000000&image=assets/images/value_reference.jpg&position=50% 75%&size=60% 60%
#### Value Types & Reference Types
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

+++
#### More data structures

```Go
var example map[string]int
example = make(map[string]int)
var myTable = map[int]string{
    0: "hi",
    1: "nice",
}
type Point struct {
	x,y float64
}
var z = map[Point]Point{
	Point{0,0}: 1,
	Point{1,1}: 0,
}
```

@[1](Go built-in maps are @color[yellow](hash tables))
@[1](zero-values of maps is nil, has no keys and @color[yellow](none can be added))
@[1-2](initialize empty map, @color[yellow](keys can be added))
@[3-6](initialize map @color[yellow](literal))
@[7-9](a map can use custom type keys)

+++
#### Structs and Interfaces

```Go
type Dog struct {
    name string
    uint age
}
type Barker interface {
	Bark() string
}
func (self Dog) Bark() string { // value receiver
    return "woof!"
}
var _ Barker = Dog{} // value won't be stored!
func (self *Dog) Bark() string { // pointer receiver
    return "woof!"
}
```

@[1-4](Structs are a collection of fields)
@[5-7](Interfaces are a set of method signatures, like an @color[yellow](abstract class))
@[5-7](Interfaces satisfaction is @color[yellow](implicit), @color[orange](NOT) like an abstract class)
@[1-7](Is Dog a Barker?)
@[1-10](Dog is a Barker)
@[8-10](self is the @color[yellow](receiver) of the @color[yellow](`Bark` method))
@[11](Compile time assert Dog is a Barker, @color[yellow](values are not stored!))
@[12-14](receivers can be @color[yellow](pointers))
@[8-14](@color[red](should we use pointer or value receivers?))

---
### Golang Goodies

+++
#### Go Escape Analysis

```Go
func foo() *struct{} {
	return &struct{}{}
}
func main() {
	var a = foo()
	fmt.Println(a) // seg fault ...?
}
$ go run -gcflags '-m -l' main.go
./main.go:6:19: &struct {} literal escapes to heap
./main.go:10:13: a escapes to heap
./main.go:10:13: main ... argument does not escape
&{}
```
@[1-7](returning a stack-allocated var?)
@[1-12](go compiler is smart and @color[yellow](allocates to heap!))

+++
#### The Empty Interface

```Go
interface {}
```

* The empty interface (interface {}) has @color[yellow](no methods)
* @color[orange](Every type) satisfies the empty interface

+++
### Concurrency

```Go
func main() {
  go expensiveComputation(x, y, z)
  anotherExpensiveComputation(a, b, c)
}
```


Roughly speaking, a goroutine is @color[yellow](like a thread), but lighter

* goroutines run concurrently (but not necessarily in parallel)
* stacks are small, segmented, sized on demand
* goroutines are muxed by demand onto true threads

+++
#### Communication via channels

```Go
chan int // type
chan<- string // send-only channel
<-chan T      // receive-only channel
var ch chan int
ch := make(chan int)
ch := make(<-chan int, 5) // 5 int buffer
ch <- 1 // send value 1 to channel ch
x = <-ch // receive a value from channel ch
close(ch)
```

@[1](type channel of ints)
@[1-3](channels can be bidirectional, send-only or receive-only)
@[1-3](channels are @color[yellow](synchronized) through gouroutines: thread-safe!)
@[4](declare a new channel var)
@[4-5](declare and @color[yellow](initialize) a new channel var)
@[4-6](declare and initialize a new channel var, @color[yellow](with a buffer))
@[7-8](send and receive operations on channels)
@[9](channels @color[yellow](need closing))

+++
#### Communicating Goroutines

```Go
func f(msg string, delay time.Duration, ch chan string) {
    for {
        ch <- msg
        time.Sleep(delay)
    }
}
func main() {
    ch := make(chan string)
    go f("A--", 500*time.Millisecond, ch)
    go f("-B-", 1001*time.Millisecond, ch)
    go f("--C", 1501*time.Millisecond, ch)

    for i := 0; i < 5; i++ {
        fmt.Println(i, <-ch)
    }
}
```

@[1-6](What does it do?)
@[1-16](What is the expected output?)


+++
#### Communicating Goroutines

```Go
...
go f("A--", 500*time.Millisecond, ch)
go f("-B-", 1001*time.Millisecond, ch)
go f("--C", 1501*time.Millisecond, ch)
...
```

```
❯ go run chan_example.go
0 --C
1 A--
2 -B-
3 A--
4 A--
```

@[6-12](Notice something?)
@[1-12](Why is `--C` printed first?)
@[1-12](Goroutine execution order is @color[yellow](not guaranteed to follow instantiation order))

+++
#### Range, close and Channels

From https://tour.golang.org/concurrency/4
```Go
func fibonacci(n int, c chan int) {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		c <- x
		x, y = y, x+y
	}
	close(c)
}
func main() {
	c := make(chan int, 10)
	go fibonacci(cap(c), c)
	for i := range c {
		fmt.Println(i)
	}
}
```
@[9-15](init channel, start @color[yellow](producer) goroutine, *range* over channel)
@[1-8](send @color[yellow](n) first fibonacci to @color[yellow](channel c), then **close**)
@[12-14](range **ends** when c is closed)

+++
#### Select
```Go
ch := make(chan int)
ch2 := make(chan int)
select {
case i := <-ch:
    // use i from ch
case i := <-ch2:
    // use i from ch2
}
select {
case i := <-ch:
    // use i
default:
    // if ch is not ready
}
```

@[1-8](`Select` allows reading from multiple channels @color[yellow](without order))
@[1-8](`Select` @color[yellow](blocks) until a channel is ready)
@[9-14](`default` case allows non-blocking channel reading)


+++
#### io.Reader & io.Writer Interfaces
```Go
type Reader interface {
    Read(p []byte) (n int, err os.Error)
}
var bufferedInput Reader = bufio.NewReader(os.Stdin)
type Writer interface {
    Write(p []byte) (n int, err os.Error)
}
func Fprintf(w Writer, fmt string, a ...interface{})
```

@[1-8](Some of the most simple and reused interfaces in standard library)
@[1-3](@color[yellow](Anything) with a `Read` method implements Reader)
@[1-4](Buffering just wraps a `Reader`)
@[1-4](Example: JPEG decoder takes a Reader, so it can decode from disk, network, gzipped HTTP, ...)
@[5-8](`Fprintf` uses a `Writer`)

+++
#### Go Tools

zero configuration, bundled tools

```
godoc <package> // web server, golang.org docs
go doc <package> // simpler cmd tool
```

```
go vet <package> // linter
```
```
go fmt <package> // format code
```
```
go test <package> // testing framework
go test -bench <package> // benchmark
go test -cover <package> // coverage report
```
```
go {build,test,install} -race <package> // race detector
```
