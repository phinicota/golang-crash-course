---?image=assets/images/Go-Logo_White.png&position=50% 20%&size=50% 50%
@title[Introduction]

<br/>
<br/>
<br/>
<br/>
An *simple* and **opinionated** statically typed compiled language.

---
## Table of Contents


* Golang Conventions
* Why is Go *NOT* Object-Oriented?
* Interfaces - io.Reader
* Basic Concurrency Patterns

---
## Golang Conventions

+++
### Why Conventions?

* Go is *opinionated*, it tries to be *batteries included*
* Some things need to force universal conventions, this:
  * maintains consistency, easier to understand other projects
  * easier to use
  * reduce development times

+++
### GOPATH

@ul
* defaults to `$HOME/go`
* @color[yellow](`$GOPATH/src`)
  * import `mypackage` searches here
  * `go get` will download code here
* @color[yellow](`$GOPATH/bin`): `go get` **commands** (needs to be in `$PATH`)
* `go env`: check vars
* most go commands refer to *package names*:
  * *go test* @color[yellow](mypackage) -> $GOPATH/src/@color[yellow](mypackage)
@ulend

+++
### GOPATH what does it look like?

```
bin/
    hello                          # command executable
    outyet                         # command executable
src/
    github.com/golang/example/
        .git/                      # Git repository metadata
	hello/
	    hello.go               # command source
	outyet/
	    main.go                # command source
	    main_test.go           # test source
	stringutil/
	    reverse.go             # package source
    ... (many more repositories and packages omitted) ...
```

+++
### Naming

@ul
* names in Go should use MixedCase
  * don't use snake_case
  * acronyms all capitals, as in Serve@color[yellow](HTTP) and @color[yellow](ID)Processor
* interfaces *usually* add **er**
  * Read() -> Reader
  * Exec() -> Execer
* exported names are @color[yellow](qualified by their package names)
  * @color[yellow](bytes.Buffer) **not** @color[orange](bytes.ByteBuffer)
  * @color[yellow](strings.Reader) **not** @color[orange](strings.StringReader)
@ulend

+++
### Packages

@ul
* Packages are low-case and, as much as possible, short: bufio, http
* Avoid stutter
  * code.google.com/p/goauth2/oauth2 <- @color[orange](bad)
* The last component of a package *path should be the same as the package name*
  * compress/gzip <- package @color[yellow](gzip)
* For libraries, put the package *code in the repo root*
  * github.com/golang/oauth2 <- package @color[yellow](oauth2)
@ulend

+++
### Complex Repositories

@ul
* `/cmd`: repo applications (e.g., `/cmd/myapp`)
* `/internal`: private application and library code.
  * you @color[orange](don't want others importing this)
* `/pkg`: library code that's ok to import
  * e.g., `/pkg/mypubliclib`
* used *subdirs* if you have more than one of each
* check [github.com/golang-standards/project-layout](https://github.com/golang-standards/project-layout)
@ulend

---
## Why is Go *NOT* Object-Oriented?

+++
### No clases, enter Structs

* more powerful than their C counterpart
* structs @color[orange](hold state)
* methods @color[orange](provide behaviour), allowing to change state
* traditional classes *combine both*

+++
### Encapsulation

* capitalized fields, methods and functions are public
* with a single look you know if something is @color[orange](public or private)
* there is not **protected**: no inheritance

+++
### Composition over Inheritance

* Software Design Principle
  * @color[yellow](inheritance): What a class **is**
  * @color[yellow](composition): What a class **does** or **has**

* **GO FAQ**: Object-oriented programming, at least in the best-known languages, *involves too much discussion of the relationships between types*, relationships that often could be derived automatically. Go takes a different approach.

[wiki/Composition_over_inheritance](https://en.wikipedia.org/wiki/Composition_over_inheritance)

+++
### Enter Struct Embedding

```Go
type Animal struct {
	Age int
}
func (a *Animal) Move() {
	fmt.Println("Animal moved")
}
func (a *Animal) SayAge() {
	fmt.Printf("Animal age: %d\n", a.Age)
}
type Dog struct {
	Animal
}
func main() {
	d := Dog{}
	d.Age = 3
	d.Move() // Animal moved
	d.SayAge() // Animal age: 3
}
```

@[1-9](Animal Struct with methods)
@[10-12](A Dog is an Animal)
@[13-18](A Dog has Animal methods)

+++
### Functions

@ul
* Go allows methods and @color[orange](functions)
* functions are types: they can be stored, passed as arguments and returned
* java/C++ static functions *workaround* the notion that *everything is an object* and function definitions must be *inside classes*
* go allows functions to live @color[orange](outside an object), just like C functions
@ulend

+++
## Interfaces

@ul
* interfaces are *typically* very small, just one method
* interfaces provide **polymorphism**
  * by accepting an interface, you declare to accept @color[orange](any kind of object satisfying that interface)
@ulend

+++
### Go's OOP

@ul
* leaving behind classes and inheritance, you’ll see @color[orange](less boilerplate)
* less reasoning about the *perfect hierarchical structure for classes* (@color[orange](hard to change))
* more freedom to @color[yellow](compose and decompose types as needed)
@ulend

---
## Interfaces

+++
### io.Reader

The io.Reader interface represents an entity from which you can *read* a stream of bytes

```Go
type Reader interface {
  Read(p []byte) (n int, err error)
}
```

+++
### io.Reader

```Go
func Open(name string) (*File, error) // File is a Reader
myfile = os.Open("file.txt") // a file is a reader
var (
  stringReader = strings.NewReader("Read will return these bytes")
  httpBody = request.Body // an http.Request body
  mybuffer bytes.Buffer
)
```

@[1-2](a file is a reader)
@[1-7](multiple types in the standard library use Reader!)

+++
### using io.Reader

```Go
myBytes := ioutil.ReadAll(r) // r is an io.Reader
n, err := io.Copy(w, r) // w is an io.Writter
err := json.NewDecoder(r).Decode(v) // v is some struct
r = gzip.NewReader(r)
```

@[1](ioutil.ReadAll reads everything from a Reader)
@[2](multiple types in the standard library use Reader!)
@[3](decode a JSON from any reader: file, string, []byte)
@[4](we can Read from gzipped data)

+++
### When should we use io.Reader?

```Go
func Reverse(s string) (string, error) // only takes string
func Reverse(r io.Reader) io.Reader // could be any Reader
// string
r = Reverse(strings.NewReader("Make me backwards"))
// file
f := os.Open("file.txt"); r = Reverse(f)

myStream := net.Listen("tcp", ":8080")
r = Reverse(myStream) // reverse streamed data through the network
```

@[1](this design Reverses only strings)
@[1-2](this design Reverses any Reader)
@[1-4](for example, a string)
@[1-6](or a File...)
@[1-9](or streamed data through a socket!)
@[1-9](use generic interfaces whenever possible!)

---
## Concurrency Patterns

+++
### Concurrency Patterns
```Go
func boring(msg string) {
    for i := 0; ; i++ {
        fmt.Println(msg, i)
        time.Sleep(time.Duration(rand.Intn(1e3))
                    * time.Millisecond)
    }
}
func main(){
    boring("boring!")
}
```

@[1-10](prints a string and sleeps a *small* random amount of time)

+++
### Generator Pattern

```Go
func boring(msg string) <-chan string { // Returns receive-only channel of strings.
    c := make(chan string)
    go func() { // We launch the goroutine from inside the function.
        defer close(c) // closes c after finishing
        for i := 0; i<5; i++ {
            c <- fmt.Sprintf("%s %d", msg, i)
            time.Sleep(time.Duration(rand.Intn(1e3))
                        * time.Millisecond)
        }
    }()
    return c // Return the channel to the caller.
}
func main(){
    joe := boring("Joe")
    ann := boring("Ann")
    for i := 0; i < 10; i++ {
        fmt.Println(<-joe)
        fmt.Println(<-ann)
    }
    fmt.Println("You're both boring; I'm leaving.")}
}
```

@[1-12](worker is responssible for its channel)
@[13-21](we only *consume* (not manage) worker channels from outside)
@[4](notice the *deferred* close channel?)

+++
### Fan-in - Multiplexing

```Go
func fanIn(input1, input2 <-chan string) <-chan string {
    c := make(chan string)
    go func() { for { c <- <-input1 } }()
    go func() { for { c <- <-input2 } }()
    return c
}
func main() {
    c := fanIn(boring("Joe"), boring("Ann"))
    for msg := range c {
        fmt.Println(msg)
    }
    fmt.Println("You're both boring; I'm leaving.")
}
```

@[7-13](what if we want to *merge* workers' output?)
@[7-13](using for range we are sure to consume *every message*)
@[1-6](fanIn worker just forwards inputs)
@[1-6](what problems does it have?)

+++
How do we implement merge()?

```Go
func merge(cs ...<-chan int) <-chan int {
    out := make(chan int)

    // Start an output goroutine for each input channel in cs.  output
    // copies values from c to out until c is closed, then calls wg.Done.
    var wg sync.WaitGroup
    output := func(c <-chan int) {
        for n := range c {
            out <- n
        }
        wg.Done()
    }
    wg.Add(len(cs))
    for _, c := range cs {
        go output(c)
    }

    // Start a goroutine to close out once all the output goroutines are
    // done.  This must start after the wg.Add call.
    go func() {
        wg.Wait()
        close(out)
    }()
    return out
}
```

@[1-2](takes a variadic amount of channels)
@[4-16](WaitGroup is used to sync workers)
@[4-16](why do we wg.Add() *before* launching goroutines?)
@[17-24](closes its channel after wg.Wait (workers finished))

+++
### Fan-out, Fan-in

```Go
func main() {
    // could be a file or even a network stream!
    source := myGenerator()

    // Distribute the work across two goroutines
    c1 := myWorker(source)
    c2 := myWorker(source)

    // Consume the merged output from c1 and c2.
    for result := range merge(c1, c2) {
        sink(result) // sink processes each result
    }
}
```

---
## End - Overview

  * Golang Conventions
  * Why is Go *NOT* Object-Oriented?
  * Interfaces - io.Reader
  * Basic Concurrency Patterns

@color[orange](Questions?)
