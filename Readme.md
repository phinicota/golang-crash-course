# Presentations

## 1st Part - Intro | [View presentation](https://gitpitch.com/phinicota/golang-crash-course/master?grs=gitlab&p=intro)

* Golang quickstart.

##### Intro References

* [GC Performance in Tweets](https://twitter.com/brianhatfield/status/692778741567721473)
* [A Taste of Go](https://talks.golang.org/2014/taste.slide)
* [Why learn go?](https://medium.com/exploring-code/why-should-you-learn-go-f607681fad65)

## 2nd Part | [View presentation](https://gitpitch.com/phinicota/golang-crash-course/master?grs=gitlab&p=2nd-part)
* Golang Conventions
* Why is Go *NOT* Object-Oriented?
* Interfaces - io.Reader
* Basic Concurrency Patterns

##### Recommended Reads

* [Example Inheritance vs Composition](https://talks.golang.org/2014/go4java.slide#32)
* [Go for C++ Programmers](https://talks.golang.org/2015/go4cpp.slide)
* [Porting dl.google.com from C++ to Go](https://talks.golang.org/2013/oscon-dl.slide)

##### Part 2 References

* [Is Go Object oriented?](https://flaviocopes.com/golang-is-go-object-oriented/)
* [gopher-vs-object-oriented-golang (detailed OOP analysis)](https://medium.com/gophersland/gopher-vs-object-oriented-golang-4fa62b88c701)
* [Project layout](https://github.com/golang-standards/project-layout)
* [Workspace](https://golang.org/doc/code.html)
* [io.Reader in depth](https://medium.com/@matryer/golang-advent-calendar-day-seventeen-io-reader-in-depth-6f744bb4320b)
* [Concurrency Patterns - Rob Pike](https://talks.golang.org/2012/concurrency.slide)

## 3rd Part - Profiling Demo

* [step-by-step profiling tutorial](https://www.integralist.co.uk/posts/profiling-go/)
* [profiling tutorial](https://jvns.ca/blog/2017/09/24/profiling-go-with-pprof/)
* [Garbage Collection mark & sweep gif](https://stackoverflow.com/a/20993931)
* [Scheduler tracing](https://www.ardanlabs.com/blog/2015/02/scheduler-tracing-in-go.html)
* [Scheduler](https://rakyll.org/scheduler/)
* [go tool trace](https://making.pusher.com/go-tool-trace/)

## General References

* [Go Branding](https://blog.golang.org/go-brand)
  * [Go mascot history (gopher)](https://blog.golang.org/gopher)
  * [gopher-vector](https://github.com/golang-samples/gopher-vector)
* [Go talks](https://github.com/golang/talks)
  *  [ISMM Keynote (on GO GC)](https://blog.golang.org/ismmkeynote)
  * Go: 90% Perfect, 100% of the time. 
    * [Fast vs Fun languages](https://talks.golang.org/2014/gocon-tokyo.slide#28) 
    * [Concurrency efficient vs beautiful languages](https://talks.golang.org/2014/gocon-tokyo.slide#31) 
    * [Get stuff done quickly vs Statically checked metaprogramming](https://talks.golang.org/2014/gocon-tokyo.slide#51) 
  * Sate of Go 2017
    * [Mutex contention profiling](https://talks.golang.org/2017/state-of-go.slide#23)
    * [GC history in tweets](https://talks.golang.org/2017/state-of-go.slide#34)
* [Visualize Concurrency](http://divan.github.io/posts/go_concurrency_visualize/) 
* Golang Pros & Cons for DevOps 
  * [Speed vs. Lack of Generics (Compilation times)](https://blog.bluematador.com/posts/golang-pros-cons-devops-part-3-speed-lack-generics/?hs_amp=true)
* Dave Cheney Presentations: [homepage](https://dave.cheney.net/about) | [repository](https://github.com/davecheney/presentations/) 
  * [five things that make go fast](https://dave.cheney.net/2014/06/07/five-things-that-make-go-fast) 
  * [Introduction to Go](https://go-talks.appspot.com/github.com/davecheney/presentations/introduction-to-go.slide) ([source](https://github.com/davecheney/presentations/blob/master/introduction-to-go.slide))
  * [Simplicity](https://go-talks.appspot.com/github.com/davecheney/presentations/simplicity.slide) ([source](https://github.com/davecheney/presentations/simplicity.slide))
  * [Concurrency made easy transcript (pdf)](https://dave.cheney.net/paste/concurrency-made-easy.pdf)
* [Russ Cox, answer to "Why I'm not leaving Python for Go" article](https://plus.google.com/+RussCox-rsc/posts/iqAiKAwP6Ce)
(Why go uses return errors instead of exceptions)
* [Rob Pike, on "Less is exponencially more"](https://commandcenter.blogspot.com/2012/06/less-is-exponentially-more.html)
(origins of Go)
* [wiki (yes, really)](https://en.wikipedia.org/wiki/Go_(programming_language))
* [No generics gif](https://qph.ec.quoracdn.net/main-qimg-dd2dc3bc72b058b85774ee804a521165)

## Exercises

* [Proposed Execises](https://gitlab.com/phinicota/golang-crash-course/tree/master/exerc)
* [Go Exercism source code](https://github.com/exercism/go/tree/master/exercises)

