# Exercises

## Go installation

1. [Download](https://golang.org/dl/)
2. [Follow install steps](https://golang.org/doc/install#install)

## Exercises

Some examples have been provided in subdirectories.

### Go Tour

If the exercises below seem to hard to grasp, you can try doing *at least* the beginning of the [Golang Tour](https://tour.golang.org/).


### Fundamentals

1. **Hello World** Make a simple hello world program that prints the time passed every second. 

  ```
  Hello World!
  waited 1.000087012s seconds...
  waited 2.000247297s seconds...
  waited 3.000373084s seconds...
  waited 4.000483121s seconds...
  waited 5.000639302s seconds...
  waited 6.000760542s seconds...
  waited 7.000913521s seconds...
  waited 8.001045724s seconds...
  waited 9.001180834s seconds...
  waited 10.001331065s seconds...
  ```

  * same, but now print messages using a goroutine.
  * same, but now use `select()` and `time.Ticker`, why not `time.Timer`? (tip: [Go by Example: Tickers](https://gobyexample.com/tickers))
  * Make main wait for goroutine signal its end (after 5 seconds). (tip: simplest channel is `chan struct{}`)
  * Make main signal *when* to print time passed (**instead of waiting**). Can you pass a message from main to the goroutine?
  * **Tips**: 
    * use `time` and `fmt` libraries; 
    * what does `%v` do in fmt.Printf()?
    * use `hello/main.go` as reference
    

2. **Concurrent-Efficient Hello World** ([Source](https://medium.com/commitlog/hello-go-3e207da92da0), *contains solutions*)
  * Check `exerc/efficient-hello/main.go`.
  * Evaluate and implement a concurrent way to do it.
  * Benchmark results.
  

### Intermediate


3. **Parsing** Use `io.Reader` to make a custom reader that "splits" the example file (`parsing/split_me.txt`) and produce the following output:
  * tip: https://gobyexample.com/reading-files)
  * tip: `io.Reader` is a low level read. `bufio.Scanner` *might* fit too.)

    ```
    Hi, this is the first message.
    This should be split into the second read.
    Message #3.
    Message #4.
    Message #5.
    Message #6.
    Message #7.
    Message #8.
    Message #9.
    Final Message.
    ```

4. [exercism](https://exercism.io/tracks/go): Choose one of the following:
  * Register and follow exercises.
  * Clone exercises and do those appealing (check [here](https://exercism.io/tracks/go/exercises))
      ```bash
      git clone https://github.com/exercism/go
      cd go/exercises/accumulate # example
      vim README.md # read exercise
      go test -v . # test solution
      ```

5. **Concurrency** (Advanced) https://tour.golang.org/concurrency/7

    (**don't cheat** solution: http://www.golangbootcamp.com/book/concurrency#sec-exercise_equiv_bin_trees)
