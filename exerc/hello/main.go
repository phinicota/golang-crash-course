package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Hello World!")

	startTime := time.Now()
	for i := 0; ; i++ {
		time.Sleep(time.Second)
		fmt.Printf("waited %v seconds...\n", time.Now().Sub(startTime))
	}
}
