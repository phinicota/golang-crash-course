package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Hello World!")
	startTime := time.Now()
	for {
		fmt.Printf("waited %v...\n", time.Now().Sub(startTime))
		time.Sleep(time.Second)
	}
}
