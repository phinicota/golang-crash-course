package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	fmt.Println("Hello World!")

	var wg sync.WaitGroup
	startTime := time.Now()
	wg.Add(1)
	go func() {
		for {
			fmt.Printf("waited %v...\n", time.Now().Sub(startTime))
			time.Sleep(time.Second)
		}
		wg.Done()
	}()
	wg.Wait()
}
