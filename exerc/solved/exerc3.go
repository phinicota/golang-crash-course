package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Hello World!")

	startTime := time.Now()
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()
	for {
		select {
		case t := <-ticker.C:
			fmt.Printf("waited %v...\n", t.Sub(startTime))
		}
	}
}
