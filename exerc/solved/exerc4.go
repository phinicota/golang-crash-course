package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	fmt.Println("Hello World!")

	startTime := time.Now()
	done := make(chan bool)
	var wg sync.WaitGroup
	go func() {
		ticker := time.NewTicker(time.Second)
		defer ticker.Stop()
		defer wg.Done()
		for {
			select {
			case <-done:
				fmt.Println("Done!")
				break
			case t := <-ticker.C:
				fmt.Printf("waited %v...\n", t.Sub(startTime))
			}
		}
	}()
	time.Sleep(5 * time.Second)
	done <- true
	wg.Wait()
}
